# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* A base project for starting any new angular project

### How do I get set up? ###

** Installing gulp **

* In the command prompt/terminal, navigate to the directory containing gulpfile.js
* Enter the following command: npm install --save-dev gulp gulp-concat gulp-uglify browser-sync

*(May need to put sudo in front of npm if using mac)*

** How to get node_modules **

* In the command prompt/terminal, enter the command: npm install

** Starting the project **

* In the command prompt/terminal, enter the command: gulp

*(this will open up a browser and it will reload anytime you make a change)*

## Who to blame? ##

** YOURSELF **