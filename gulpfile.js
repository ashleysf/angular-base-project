var gulp = require('gulp');
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');
var browserSync = require('browser-sync');


var paths = [
    './js/*.js',
    './js/**/*module.js',
    './js/**/!(*module).js',
    './js/**/**/*.js'
];

gulp.task('default', ['watch', 'browser-sync', 'concat'], function() {
    gulp.watch("css/*.css", ['bs-reload']);
    gulp.watch("*.html", ['bs-reload']);
    gulp.watch(paths, ['bs-reload']);
});

gulp.task('watch', function() {
    gulp.watch(paths, ['concat']);
});

gulp.task('concat', function() {
    return gulp.src(paths)
        .pipe(concat('prod-min.js'))
        .pipe(uglify({
            mangle : false
        }))
        .pipe(gulp.dest('./dist/'))
        .pipe(browserSync.reload({stream:true}));
});

gulp.task('browser-sync', function() {
    browserSync({
        server: {
            baseDir: "./"
        }
    });
});

gulp.task('bs-reload', function () {
    browserSync.reload();
});
